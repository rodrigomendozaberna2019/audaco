import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faWhatsapp} from "@fortawesome/free-brands-svg-icons"

const WhatsApp = () => (
    <a href="https://wa.me/528129747995?text=Gracias por contactarnos"
       className="whatsapp"
       target="_blank">
        <FontAwesomeIcon icon={faWhatsapp}/>
    </a>

);

export default WhatsApp;
