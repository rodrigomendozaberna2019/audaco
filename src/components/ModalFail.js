import React from "react";
import {Link} from "gatsby";

function ModalSuccess({show, onClose}) {
    if (!show) {
        return null;
    }

    const refreshPage = () => {
        window.location.reload(false);
    }

    return (
        <div className="modal-overlay">
            <div className="modal">
                <div className="modal-content">
                    <img style={{paddingBottom: 20}} src={require('../assets/img/fail.svg')} width={150}
                         height='auto'/>
                    <h1 className="services_section_1__title" style={{color: '#000000'}}>Mensaje no enviado</h1>
                    <p className="services_section_1__paragraph">Hubo un problema al enviar el correo, intenta de
                        nuevo</p>
                    <button className="button" onClick={() => refreshPage()}>De acuerdo</button>
                </div>
            </div>
        </div>
    )
}

export default ModalSuccess;
